function getData() {
    return new Promise((resolve) => {       
        var xhr = new XMLHttpRequest();//обращение к серверу
        xhr.open('GET', '../data.json', true);//сконфигурировали запрос
        xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {                
                resolve(JSON.parse(xhr.responseText));
            }
        } 
    }
    xhr.send();
    });    
}

var data = getData();
//data.then(a => console.log(a));
//console.log(data);

//getData();

function temporary() {
    return new  Promise((resolve) => {
        setTimeout(() => {
            resolve (5);
        }, 2000);
    });    
}

var resault = temporary();
resault.then(function(param) {
    console.log(param);
})

var dishCounter = 1;
var selectedDish;
var cart = [];

const CATEGORIES = 
[
    {
        id: 0,
        name:'Первые блюда',
    },

    {
        id: 1,
        name:'Вторые блюда',
    },

    {
        id: 2,
        name:'Напитки',
    },

    {
        id: 3,
        name:'Десерт',
    },
];

function generateFilterButtons () {
    var filter = get('filter');
    CATEGORIES.forEach(category => {
        var button = document.createElement('div');
        button.className = 'filter__button';
        button.innerText = category.name;
        button.addEventListener('click', () => {
            filteredDishes(category.id);
        });
        filter.appendChild(button);
    });
}

function filteredDishes(id) {
    var result = allDishes.filter(item => item.category == id);
    generatedDishes(result);
}

function generatedDish(item) {
    var content = get('content__menu');
    var dish = `<div class="dish">
                    <img class="dish__img" src="./image/${item.image}" alt="">
                    <div class="dish__info">
                        <div class="dish__info-text">${item.name}<br>${item.description}</div>
                        <div class="dish__info-control">
                            <span>${item.price}:руб</span>
                            <div class="dish__info-control-button" onclick="buy(${item.id})">Купить</div>
                        </div>
                    </div>
                </div>`
    content.innerHTML += dish;//добавляет в конец страницы
}

function buy (itemId) {
    console.log(itemId);
    selectedDish = allDishes.find(item => item.id == itemId);
    displayOrder(selectedDish);
    getTotalSumm(selectedDish);
    console.log(selectedDish);
    openModal('modal');

}

function generatedDishes(dishes) {
    var content = get('content__menu');
    content.innerHTML = '';//чистим старый контент при каждом вызове.
    dishes.forEach(dish => {
        generatedDish(dish);
    });
    
}

function displayOrder(product) {
    const item = get('modal__window-item');
    const card = `<div class="info">
                    <img class="info__img" src="./image/${product.image}" alt="">
                    <div class="info__description">${product.description}</div>
                  </div>
                <div class="action">
                    <div class="action__price">
                        <span>Цена товара: <b>${product.price}.руб</b></span>
                        <span >Общая стоимость: <b class="total-summ">2.руб</b></span>
                    </div>
                    <div class="action__confirm">
                        <div class="counter">
                            <span class="counter__action" onclick="decrementCounter()">-</span>
                            <input class="counter__inp" type="text" onkeyup="checkInput(this, event)">
                            <span class="counter__action" onclick="incrementCounter()">+</span>
                        </div>
                        <div class="confirm-button" onclick="confirmOrder()">Подтвердить заказ</div>
                    </div>
                </div>`;
    item.innerHTML = card;
}
function confirmOrder() {
    const counter = {
        count: +get('counter__inp').value
    };
    Object.assign(selectedDish, counter);//в объект selectedDish записываем поле соunter  из объекта count
    const isHaveItem = cart.some(elem => elem.id == selectedDish.id);
    console.log(isHaveItem);
    if (isHaveItem) {
        const item = cart.find(elem => elem.id == selectedDish.id);
        item.count += counter.count;
    }else {
        cart.push(selectedDish);//добавляет элемент в конец массива cart
    }
    
    closeModal('modal');
    console.log(cart);
    getCartSumm();
    
}

function getCartSumm() {
    var summ = 0;
    cart.forEach(dish => {
        summ += dish.price * dish.count;
    });
    get('header__price').innerText = Math.round(summ * 100)/100;
}
    /*allDishes.forEach((element, index) => {
        setTimeout(() => {
            console.log(`Элемент с индексом: ${index} равен: ${element}`);
        }, 500);//500- время задержки.
    });
}*/

function checkInput (input, event) {
    
    if(+event.key >= 0 && +event.key < 10) {
        if(input.value > 10) input.value = 10;
        return;
    }
    input.value = 1;
}

function getTotalSumm(dish) {
    const summ = dish.price * dishCounter;
    const totalSumm = get('total-summ');
    totalSumm.innerText = Math.round(summ*100)/100;//round-округляет до целого по правилам матем(*100/100-передает два знака после запятой. floor-округ. в меньшую сторону. ceil-в большую сторону.
}

function incrementCounter() {
    if (dishCounter<10) {
        dishCounter++;
    }
    sendCounter();   
    getTotalSumm(selectedDish); 
}
function decrementCounter() {
    if(dishCounter>2){
        dishCounter--;
    }  
    sendCounter();  
    getTotalSumm(selectedDish);  
}

function sendCounter() {
    const input = get('counter__inp');
    input.value = dishCounter;
}


/*
function exit() {
    var ex = get('fas fa-times');
    //ex.style.height = '120px';
    ex.addEventListener('click', () => {
        ModEx = get('modal');
        ModEx.style.height = '0';
        ModEx.style.width = '0';
        ModEx.style.relative;
        ModEx.style.right = '-20px';
        ModEx.style.top = '-20px';
    });
}*/

function closeModal (elem) {
    const modal = get(elem);
    modal.style.display = 'none';
    dishCounter = 1;
}
function openModal (elem) {
    if(elem == 'cart-modal') {
        showCartItems();
    }
    const modal = get(elem);
    modal.style.display = 'flex';
    sendCounter();

}
function help (event)//event-все что происходит по нажатию
{
    console.log(event);
    event.stopPropagation();//останавливает распространение событие на вложенные(дочерние элементы)
}

function showCartItems() {
    const cart_area = get('cart-modal__window');
    cart_area.innerHTML = '';
    cart.forEach(elem => {
        const item = `<div class="cart-item">
                        <div class="cart-item__info">
                            <span class="cart-item__info-name" title="${elem.name}">${elem.name}</span>
                            Цена:<span class="cart-item__info-price">${elem.price}</span>
                            Кол-во: <span class="cart-item__info-count">${elem.count}</span>
                        </div>
                        <i class="far fa-times-circle cart-item__delete" onclick="deleteItem(${elem.id})"></i>
                        </div>`;
        cart_area.innerHTML += item;
    });
}
//удаляем блюдо из корзины метод-1
/*function deleteItem(id) {
    //console.log(cart.splice(0, 1));
    //console.log(cart);  
    const dish = cart.find(elem => elem.id == id);
    const index = cart.indexOf(dish);
    cart.splice(index, 1);
    showCartItems();
    //console.log('Корзина после удаления блюда: ', cart);
}*/
//удаляем блюдо из корзины метод-2
function deleteItem (id) {
    cart = cart.filter(elem => elem.id != id);
    showCartItems();
}


var get = function (name) {
    return document.getElementsByClassName(name)[0];
}

var selectedCategryId = CATEGORIES[0].id;
var allDishes;

window.onload = function() {
    generateFilterButtons();
    
    data.then(result => {
        //var dishes = result.filter(item => item.category == selectedCategoryId);
        //console.log(dishes);
        console.log(result);        
        allDishes = result;
        var filteredDishes = allDishes.filter(item => item.category == selectedCategryId);
        generatedDishes(filteredDishes);
        //exit();
        //getCartSumm();
        
});}


//сделать функцию вывода на консоль стоимости покупки
